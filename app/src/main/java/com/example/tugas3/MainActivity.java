package com.example.tugas3;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private HashMap<CheckBox, ImageView> checkBoxImageViewMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CheckBox checkBox3 = findViewById(R.id.checkBox3);
        ImageView imageView11 = findViewById(R.id.imageView11);
        checkBoxImageViewMap.put(checkBox3, imageView11);

        CheckBox checkBox4 = findViewById(R.id.checkBox4);
        ImageView imageView6 = findViewById(R.id.imageView6);
        checkBoxImageViewMap.put(checkBox4, imageView6);

        CheckBox checkBox2 = findViewById(R.id.checkBox2);
        ImageView imageView9 = findViewById(R.id.imageView9);
        checkBoxImageViewMap.put(checkBox2, imageView9);

        CheckBox checkBox5 = findViewById(R.id.checkBox5);
        ImageView imageView10 = findViewById(R.id.imageView10);
        checkBoxImageViewMap.put(checkBox5, imageView10);

        for (final CheckBox checkBox : checkBoxImageViewMap.keySet()) {
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ImageView imageView = checkBoxImageViewMap.get(checkBox);
                    if (checkBox.isChecked()) {
                        imageView.setVisibility(View.VISIBLE);
                    } else {
                        imageView.setVisibility(View.GONE);
                    }
                }
            });
        }
    }
}